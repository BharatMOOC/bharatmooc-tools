A Note
======

Please visit the [bharatmooc-tools wiki](https://github.com/bharatmooc/bharatmooc-tools/wiki) for a more complete listing of the tools shared by the larger BharatMOOC community, including an index of other repositories.

***Dear Contributors: Please reflect updates that you make to the contents of this repository on the public wiki page, as appropriate.***

----

bharatmooc-tools README.md
===================

A collection of miscellaneous tools for use with the BharatMOOC platform.

These tools might not be production quality - they may not be reviewed, may not do
error-handling, etc.  Use at your own risk.  Please also feel free to contribute tools
that are useful to you.  Almost anything will get merged.

You could imagine a scenario where something that starts out used informally from this
repo eventually gets adopted into the bharatmooc-platform repo.  But not everything will have to
go that route.

Current directories include:
 - analytics (tools relevant to research and analysis of BharatMOOC)
 - captions (for tools related to captions)
 - logging (for tools related to logging)
 - transcoding (tools to assist with transcoding of videos on AWS)
 - branch-drift (tools related to github and difference between branches and master)
 - and others, as described on the [bharatmooc-tools wiki](https://github.com/bharatmooc/bharatmooc-tools/wiki).

---
 

