Simple tool to map IP addresses to countries as listed in the GeoLite
data created by MaxMind GeoLite database.  Database available from
<http://www.maxmind.com>.  Assumes that the database, named GeoIP.dat,
is in this directory.  It is checked in with the bharatmooc source 
here: [bharatmooc-platform/common/static/data/geoip/GeoIP.dat][gh].

  [gh]: https://github.com/bharatmooc/bharatmooc-platform/raw/master/common/static/data/geoip/GeoIP.dat

